#include "map_gen.h"
#include <iostream>
#include <vector>
#include <random>
#include <fstream>

using namespace std;
using namespace bsp;

int main()
{
	BSPtree tree = BSPtree();
	tree.ReadParams("params.txt");
	tree.GenerateMap();
	tree.ShowMap();
	tree.WriteInFile("example.txt");
	return 0;
}