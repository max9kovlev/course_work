/*! \file map_gen.cpp
 * \brief map generation functions using bsp-tree (implementations)
 */
#include "map_gen.h"
#include <iostream>
#include <vector>
#include <random>
#include <fstream>

using namespace std;
using namespace bsp;

namespace bsp 
{


	Unit::Unit(int x, int y) 
	{
		this->x = x;
		this->y = y;
	}

	Unit::Unit()
	{
		x = 0;
		y = 0;
	}

	Rect::Rect() 
	{
		x = 0;
		y = 0;
		w = 0;
		h = 0;
	}

	Rect::Rect(int X, int Y, int W, int H)
	{
		this->x = X;
		this->y = Y;
		this->w = W;
		this->h = H;
	}

	void Rect::Draw(char a, vector<vector<Unit*>>& map)
	{
		for (int i = x; i < x + w; i++)
			for (int j = y; j < y + h; j++)
				map[i][j]->a = a;
	}

	Leaf::Leaf(int X, int Y, int W, int H)
	{
		this->x = X;
		this->y = Y;
		this->h = H;
		this->w = W;
		left = nullptr;
		right = nullptr;
		room = nullptr;

		if (RandomSplit())
		{
			if ((this->left != nullptr) && (this->right != nullptr))
			{
				CreateCorridor(this->left->GetRoom(), this->right->GetRoom(), corridors);
			}
		}
		else
		{
			int leftmargin = rand() % maxRoomMargin + 1;
			int rightmargin = rand() % maxRoomMargin + 1;
			int topmargin = rand() % maxRoomMargin + 1;
			int bottommargin = rand() % maxRoomMargin + 1;
			int x = this->x + leftmargin;
			int y = this->y + topmargin;
			int w = this->w - leftmargin - rightmargin;
			int h = this->h - topmargin - bottommargin;
			room = new Rect(x, y, w, h);
		}
	}

	void Leaf::DrawCorridors(vector<vector<Unit*>>& map)
	{

		if ((this->left != nullptr) && (this->right != nullptr)) 
		{
			//cout << corridors.size();
			for (int i = 0; i < corridors.size(); i++)
				corridors[i]->Draw(corridorChar, map);
			this->left->DrawCorridors(map);
			this->right->DrawCorridors(map);
		}
	}

	void Leaf::DrawRoom(vector<vector<Unit*>>& map)
	{
		if (this->room != nullptr)
			this->room->Draw(roomChar, map);
		else
		{
			this->left->DrawRoom(map);
			this->right->DrawRoom(map);
		}
	}

	void Leaf::CreateCorridor(Rect* l, Rect* r, vector<Rect*>& corridors)
	{
		int x1 = rand() % l->w + l->x;
		int x2 = rand() % r->w + r->x;
		int y1 = rand() % l->h + l->y;
		int y2 = rand() % r->h + r->y;
		w = x2 - x1;
		h = y2 - y1;
		if (w < 0)
		{
			if (h < 0)
			{
				if (rand() % 2 == 0)
				{
					corridors.push_back(new Rect(x2, y1, abs(w), corridorWidth));
					corridors.push_back(new Rect(x2, y2, corridorWidth, abs(h) + corridorWidth));
				}
				else
				{
					corridors.push_back(new Rect(x2, y2, abs(w) + corridorWidth, corridorWidth));
					corridors.push_back(new Rect(x1, y2, corridorWidth, abs(h)));
				}
			}
			else if (h > 0)
			{
				if (rand() % 2 == 0)
				{
					corridors.push_back(new Rect(x2, y1, abs(w), corridorWidth));
					corridors.push_back(new Rect(x2, y1, corridorWidth, abs(h) + corridorWidth));
				}
				else
				{
					corridors.push_back(new Rect(x2, y2, abs(w) + corridorWidth, corridorWidth));
					corridors.push_back(new Rect(x1, y1, corridorWidth, abs(h)));
				}
			}
			else 
			{
				corridors.push_back(new Rect(x2, y2, abs(w), corridorWidth));
			}
		}
		else if (w > 0)
		{
			if (h < 0)
			{
				if (rand() % 2 == 0)
				{
					corridors.push_back(new Rect(x1, y2, abs(w), corridorWidth));
					corridors.push_back(new Rect(x1, y2, corridorWidth, abs(h)));
				}
				else
				{
					corridors.push_back(new Rect(x1, y1, abs(w) + corridorWidth, corridorWidth));
					corridors.push_back(new Rect(x2, y2, corridorWidth, abs(h) + corridorWidth));
				}
			}
			else if (h > 0)
			{
				if (rand() % 2 == 0)
				{
					corridors.push_back(new Rect(x1, y1, abs(w) + corridorWidth, corridorWidth));
					corridors.push_back(new Rect(x2, y1, corridorWidth, abs(h)));
				}
				else
				{
					corridors.push_back(new Rect(x1, y2, abs(w), corridorWidth));
					corridors.push_back(new Rect(x1, y1, corridorWidth, abs(h) + corridorWidth));
				}
			}
			else // if (h == 0)
			{
				corridors.push_back(new Rect(x1, y1, abs(w), corridorWidth));
			}
		}
		else // if (w == 0)
		{
			if (h < 0)
			{
				corridors.push_back(new Rect(x2, y2, corridorWidth, abs(h)));
			}
			else if (h > 0)
			{
				corridors.push_back(new Rect(x1, y1, corridorWidth, abs(h)));
			}
		}
	}

	Rect* Leaf::GetRoom()
	{
		if (this->room != nullptr)
		{
			return this->room;
		}
		else
		{
			Rect* leftroom = nullptr;
			Rect* rightroom = nullptr;
			if (this->left != nullptr)
			{
				leftroom = this->left->GetRoom();
			}
			if (this->right != nullptr)
			{
				rightroom = this->right->GetRoom();
			}
			if (leftroom == nullptr && rightroom == nullptr)
				return nullptr;
			else if (rightroom == nullptr)
				return leftroom;
			else if (leftroom == nullptr)
				return rightroom;
			else if (rand() % 2 == 0)
				return leftroom;
			else
				return rightroom;
		}
	}

	bool Leaf::RandomSplit()
	{
		int s = rand() % 3;
		if (s == 0)
		{
			if ((this->w > 2 * minLeafSize) && ((2 * this->h) / this->w <= maxRoomLeafRatio))
			{
				HorizontalSplit();
				return true;
			}
			else if ((this->h > 2 * minLeafSize) && ((2 * this->w) / this->h <= maxRoomLeafRatio))
			{
				VerticalSplit();
				return true;
			}
			else return false;
		}
		else if (s == 1)
		{
			if ((this->h > 2 * minLeafSize) && ((2 * this->w) / this->h <= maxRoomLeafRatio))
			{
				VerticalSplit();
				return true;
			}
			else if ((this->w > 2 * minLeafSize) && ((2 * this->h) / this->w <= maxRoomLeafRatio))
			{
				HorizontalSplit();
				return true;
			}
			else return false;
		}
		else
		{
			if (this->h > 4 * minLeafSize)
			{
				VerticalSplit();
				return true;
			}
			else if (this->w > 4 * minLeafSize)
			{
				HorizontalSplit();
				return true;
			}
			else return false;
		}
	}

	void Leaf::HorizontalSplit()
	{
		int w1 = rand() % (this->w - 2 * minLeafSize) + minLeafSize;
		int w2 = this->w - w1 - 1;
		int x2 = this->x + w1 + 1;
		this->left = new Leaf(this->x, this->y, w1, this->h);
		this->right = new Leaf(x2, this->y, w2, this->h);
	}

	void Leaf::VerticalSplit()
	{
		int h1 = rand() % (this->h - 2 * minLeafSize) + minLeafSize;
		int h2 = this->h - h1 - 1;
		int y2 = this->y + h1 + 1;
		this->left = new Leaf(this->x, this->y, this->w, h1);
		this->right = new Leaf(this->x, y2, this->w, h2);
	}

	BSPtree::BSPtree() = default;
	void BSPtree::GenerateMap()
	{
		for (int i = 0; i < mapWidth; i++)
		{
			map.push_back(vector<Unit*>());
			for (int j = 0; j < mapHeight; j++)
			{
				map[i].push_back(new Unit(i, j));
				map[i][j]->a = borderChar;
			}
		}

		
		root = new Leaf(0, 0, mapWidth, mapHeight);
		root->DrawCorridors(map);
		root->DrawRoom(map);
		

	}

	void BSPtree::WriteInFile(string path)
	{
		ofstream file;
		file.open(path);
		for (int i = 0; i < map.size(); ++i) {
			for (int j = 0; j < map[i].size(); ++j) {
				file << map[i][j]->a;
			}
			file << "\n";
		}
		file.close();
	}

	void BSPtree::ReadParams(string path)
	{
		ifstream file(path);
		string param_name;
		file >> param_name >> minLeafSize;
		file >> param_name >> maxRoomMargin;
		file >> param_name >> maxRoomLeafRatio;
		file >> param_name >> corridorWidth;
		file >> param_name >> mapWidth;
		file >> param_name >> mapHeight;
		file >> param_name >> roomChar;
		file >> param_name >> corridorChar;
		file >> param_name >> borderChar;
		file.close();
	}

	void BSPtree::ShowMap()
	{
		for (int i = 0; i < mapWidth; i++)
		{
			for (int j = 0; j < mapHeight; j++)
			{
				cout << map[i][j]->a;
			}
			cout << endl;
		}
	}
}

