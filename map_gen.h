/*! \file map_gen.h
 * \brief map generation functions using bsp-tree
 */
#pragma once
#include <iostream>
#include <vector>
#include <random>

using namespace std;

namespace bsp
{
	static int minLeafSize = 4;					//!<  min size of leaf
	static int maxRoomMargin = 1;				//!<  max room margin
	static int maxRoomLeafRatio = 1;			//!<  max ratio of room/leaf sides
	static int corridorWidth = 1;				//!<  width of corridors between rooms
	static int mapWidth = 40;					//!<  map width
	static int mapHeight = 60;					//!<  map height

	static char roomChar;						//!< symbol that room is encoded
	static char corridorChar;					//!< symbol that corridor is encoded
	static char borderChar;						//!< symbol that border is encoded

	///
	/// \brief Class of unit - single element of map
	///
	class Unit 
	{
	public:
		Unit(int x, int y);
		Unit();
		char a = ' ';
	private:
		int x, y;
	};

	///
	/// \brief Class of rectangle. Partitions, rooms and halls represented by rectangles.
	///
	class Rect 
	{
	public:
		int x, y, h, w;
		Rect();
		Rect(int X, int Y, int W, int H);
		void Draw(char a, vector<vector<Unit*>>& map); //!< Draw function of rectangle
	};

	///
	/// \brief Class of leaf. BSP-tree implemented by this class using random splits 
	///
	class Leaf 
	{
	public:
		Leaf* left = nullptr;
		Leaf* right = nullptr;
		Rect* room = nullptr;
		vector<Rect*> corridors;

		Leaf(int X, int Y, int W, int H); 
		void DrawCorridors(vector<vector<Unit*>>& map); //!< Drawing corridors on map
		void DrawRoom(vector<vector<Unit*>>& map); //!< Drawing room on map
		void CreateCorridor(Rect* l, Rect* r,  vector<Rect*>& corridors); //!< Creates corridors between two rooms
		Rect* GetRoom();
		bool RandomSplit(); //!< Binary partition random split

	private:
		int x, y, h, w;
		void HorizontalSplit(); //!< Inner split function
		void VerticalSplit(); //!< Inner split function
	};


	///
	/// \brief Class of BSPtree.  
	///
	class BSPtree
	{
	public:
		BSPtree();
		void GenerateMap(); //!< Generates map
		void ReadParams(string path); //!< Reads params from file
		void WriteInFile(string path); //!< Writes map to file
		void ShowMap(); //!< Show map in console
		vector<vector<Unit*>> map;
		Leaf* root = nullptr; //!< Root of the tree
		
	};
}
